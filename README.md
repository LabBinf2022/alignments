Project Title
Bioinformatics Pipeline with Snakemake

Description
This project contains a bioinformatics pipeline built with Snakemake, which automates the process of generating multiple sequence alignments for a set of fasta files. The pipeline installs and uses various bioinformatics tools such as MAFFT, ClustalW, and MUSCLE to align the sequences, and summarizes the alignment results with the help of a Python script. The pipeline is containerized using Docker, making it easy to deploy and run on any system.

-----Prerequisites

Docker
Input fasta files




-----Installation

To use this pipeline, you need to have Docker installed on your system. After that, you can simply pull the Docker image from DockerHub using the following command:

docker pull snakemake/snakemake:latest


-----Usage
1. Clone the repository to your local system:

git clone https://github.com/<username>/<repository>

2. Copy your input fasta files to the input directory.

3. Build the Docker image using the following command:

docker build -t <image_name> .

4. Run the pipeline with the following command:

docker compose up 

This command will run the Snakemake pipeline with the specified options and generate output files in the output directory.

5. Compare Results


-----Files

---Dockerfile
This file defines the Docker image that the pipeline will run on. It installs the necessary bioinformatics tools and Python packages.

---Snakefile
This file contains the rules for the pipeline. It defines the steps that will be executed to generate the multiple sequence alignments.

---summarize_alignment.py

This Python file contains a script that performs analysis on multiple sequence alignments of nucleotide or protein sequences in FASTA format. It calculates and reports several statistics on the alignment, including:

Sum of pairs (SP) scores
Average percentage identity


The script requires the following modules to be imported:

Bio.AlignIO from Biopython: used to read and write multiple sequence alignments in various file formats.
numpy as np: used for numerical calculations.
argparse: used for command line argument parsing.
Bio.Align.AlignInfo: used to calculate the consensus sequence and consensus score for a multiple sequence alignment.



Command Line Arguments
The script uses argparse to accept command line arguments. The user must specify at least one input FASTA file using the -f or --fasta argument. This argument can accept multiple input files by specifying them as separate arguments. For example, to analyze two files file1.fasta and file2.fasta, the user would run:

Copy code
python script.py -f file1.fasta file2.fasta
Functions
The script defines three functions:

read_fasta(file_path)
This function reads in a FASTA file and returns a list of sequences. It takes one argument:

file_path: the path to the input FASTA file.
percentage_identity(seq1, seq2)
This function calculates the percentage identity between two sequences by comparing each corresponding pair of characters in the two sequences. It takes two arguments:

seq1: the first sequence to be compared.
seq2: the second sequence to be compared.
average_percentage_identity(sequences)
This function calculates the average percentage identity between all pairs of sequences in a list of sequences. It takes one argument:

sequences: a list of sequences to be compared.
Main Script
The main script starts by parsing the command line arguments using argparse. It then loops over each input file, reading it into a MultipleSeqAlignment object using Bio.AlignIO.read(). If the file is in Clustal format (.aln extension), it uses the "clustal" format argument; otherwise, it assumes the file is in FASTA format.

Next, it calculates the SP score and total column score for each column in the alignment. The SP score is the number of pairs in the column that have no gaps ("-"). The total column score is the total number of non-gap characters in the column. These scores are used to calculate various statistics on the alignment, including the minimum, maximum, average, standard deviation, and median SP and total column scores.

The script also calculates the percentage identity between all pairs of sequences in the alignment. If the input file is in FASTA format (not Clustal format), the script reads in the sequences using read_fasta() and calculates the average percentage identity between all pairs of sequences using average_percentage_identity().

Finally, the script writes the output to a text file named alignment_report_{filename}.txt, where filename is the name of the input file. The output includes the SP score statistics and average percentage identity.

The script also prints the SP score and average percentage identity for each input file to the console.