'''
Introduction
This Python file contains a script that performs analysis on multiple sequence alignments of nucleotide or protein sequences in FASTA format. It calculates and reports several statistics on the alignment, including:

Sum of pairs (SP) scores
Average percentage identity


The script requires the following modules to be imported:

Bio.AlignIO from Biopython: used to read and write multiple sequence alignments in various file formats.
numpy as np: used for numerical calculations.
argparse: used for command line argument parsing.
Bio.Align.AlignInfo: used to calculate the consensus sequence and consensus score for a multiple sequence alignment.



Command Line Arguments
The script uses argparse to accept command line arguments. The user must specify at least one input FASTA file using the -f or --fasta argument. This argument can accept multiple input files by specifying them as separate arguments. For example, to analyze two files file1.fasta and file2.fasta, the user would run:

Copy code
python script.py -f file1.fasta file2.fasta
Functions
The script defines three functions:

read_fasta(file_path)
This function reads in a FASTA file and returns a list of sequences. It takes one argument:

file_path: the path to the input FASTA file.
percentage_identity(seq1, seq2)
This function calculates the percentage identity between two sequences by comparing each corresponding pair of characters in the two sequences. It takes two arguments:

seq1: the first sequence to be compared.
seq2: the second sequence to be compared.
average_percentage_identity(sequences)
This function calculates the average percentage identity between all pairs of sequences in a list of sequences. It takes one argument:

sequences: a list of sequences to be compared.
Main Script
The main script starts by parsing the command line arguments using argparse. It then loops over each input file, reading it into a MultipleSeqAlignment object using Bio.AlignIO.read(). If the file is in Clustal format (.aln extension), it uses the "clustal" format argument; otherwise, it assumes the file is in FASTA format.

Next, it calculates the SP score and total column score for each column in the alignment. The SP score is the number of pairs in the column that have no gaps ("-"). The total column score is the total number of non-gap characters in the column. These scores are used to calculate various statistics on the alignment, including the minimum, maximum, average, standard deviation, and median SP and total column scores.

The script also calculates the percentage identity between all pairs of sequences in the alignment. If the input file is in FASTA format (not Clustal format), the script reads in the sequences using read_fasta() and calculates the average percentage identity between all pairs of sequences using average_percentage_identity().

Finally, the script writes the output to a text file named alignment_report_{filename}.txt, where filename is the name of the input file. The output includes the SP score statistics and average percentage identity.

The script also prints the SP score and average percentage identity for each input file to the console.

'''


from Bio import AlignIO
import numpy as np
import argparse
from Bio.Align import AlignInfo

ap = argparse.ArgumentParser()
ap.add_argument("-f", "--fasta", required=True, nargs='+',help="path to .fasta file")
args = vars(ap.parse_args())


def read_fasta(file_path):
    sequences = []
    with open(file_path, 'r') as f:
        sequence = ''
        for line in f:
            if line.startswith('>'):
                if sequence:
                    sequences.append(sequence)
                    sequence = ''
            else:
                sequence += line.strip()
        sequences.append(sequence)
    return sequences

# Calculate the percentage identity
def percentage_identity(seq1, seq2):
    matches = 0
    total = 0
    for base1, base2 in zip(seq1, seq2):
        if base1 != '-' and base2 != '-':
            total += 1
            if base1 == base2:
                matches += 1
    return 100 * matches / total

# Calculate the average percentage identity
def average_percentage_identity(sequences):
    total_identity = 0
    count = 0
    for i in range(len(sequences)):
        for j in range(i+1, len(sequences)):
            identity = percentage_identity(sequences[i], sequences[j])
            total_identity += identity
            count += 1
    return total_identity / count

for fasta in args["fasta"]:
    if fasta == 'clustawl.aln':
        alignment = AlignIO.read(str(fasta), "clustal")
    else:
        alignment = AlignIO.read(str(fasta), "fasta")

    sp_scores = []
    total_column_scores = []
    for column in range(alignment.get_alignment_length()):
        col = alignment[:, column]
        sp_score = sum(1 for pair in col if pair.count('-') == 0)
        total_column_score = len(col) - col.count('-')
        sp_scores.append(sp_score)
        total_column_scores.append(total_column_score)

    # Calculate statistics
    min_sp = np.min(sp_scores)
    max_sp = np.max(sp_scores)
    avg_sp = np.mean(sp_scores)
    std_sp = np.std(sp_scores)
    median_sp = np.median(sp_scores)

    min_total_column = np.min(total_column_scores)
    max_total_column = np.max(total_column_scores)
    avg_total_column = np.mean(total_column_scores)
    std_total_column = np.std(total_column_scores)
    median_total_column = np.median(total_column_scores)
    num_seqs = len(alignment)
    alignment_len = alignment.get_alignment_length()

    total_matches = sum([1 for i in range(alignment_len) if len(set(alignment[:,i])) == 1])
    identity = total_matches / alignment_len * 100


    if fasta != 'clustawl.aln':
        sequences = read_fasta(fasta)
        average_identity = average_percentage_identity(sequences)










    # Write results to file
    with open(f"alignment_report_{fasta}.txt", "w") as f:
        f.write(f"Sum of pairs scores for :\n")
        f.write(f"Minimum: {min_sp}\n")
        f.write(f"Maximum: {max_sp}\n")
        f.write(f"Average: {avg_sp}\n")  # Display as percentage
        f.write(f"Standard deviation: {std_sp}\n")
        f.write(f"Median: {median_sp}\n")
        f.write(f"Average percentage identity:  {average_identity:.2f}%\n")
        # f.write(f"Total column scores for:\n")
        # f.write(f"Minimum: {min_total_column}\n")
        # f.write(f"Maximum: {max_total_column}\n")
        # f.write(f"Average: {avg_total_column}\n")
        # f.write(f"Standard deviation: {std_total_column}\n")
        # f.write(f"Median: {median_total_column}\n")

    print(f'{fasta}:')
    print(f"    Sum of pairs score: {avg_sp}")  # Display as percentage
    # print(f"    Percentage identity: {identity:.2f}%\n")
    print(f"    Average percentage identity: {average_identity:.2f}")

# Calculate overall average Sum of Pairs score as percentage
