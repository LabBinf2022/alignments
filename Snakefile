

#This is the top-level rule and references four other rules: mafft, clustawl, muscle, and muscle_fastest.
# It also expands to an additional set of input files.
rule all:
    input:
        "mafft.fasta",
        "clustawl.aln",
        "muscle.fasta",
        "muscle-fastest.fasta",
        expand("{dataset}", dataset=["alignment_report_clustawl.aln.txt","alignment_report_mafft.fasta.txt","alignment_report_muscle.fasta.txt","alignment_report_muscle-fastest.fasta.txt"])

#This rule runs the mafft command on the given input file and generates a temporary mafft.fasta output
rule mafft:
    input:
        "input.fasta"
    output:
        temp("mafft.fasta")
    shell:
        "mafft --thread -6 input.fasta > mafft.fasta"


#This rule runs the clustalw command on the given input file and generates a temporary clustawl.aln output.
rule clustalw:
    input:
        "input.fasta"
    output:
        temp("clustawl.aln")
    shell:
        "clustalw -infile=input.fasta -outfile=clustawl.aln"


#This rule runs the muscle command on the given input file and generates a temporary muscle.fasta output.
rule muscle:
    input:
        "input.fasta"
    output:
        temp("muscle.fasta")
    shell:
        "muscle -in input.fasta -out muscle.fasta"



#This rule runs the same muscle command as above, but with the maxiters 1 -diags flag enabled.
# This will produce a faster result. It generates a temporary muscle-fastest.fasta output.
rule muscle_fastest:
    input:
        "input.fasta"
    output:
        temp("muscle-fastest.fasta")
    shell:
        "muscle -in input.fasta -out muscle-fastest.fasta -maxiters 1 -diags"


# This rule will run the summarize_alignment.py script on each of the fasta files listed in the input block,
# and produce a new report file for each named accordingly and listed in the output block.

rule summarize_alignment:
    input:
        expand("{dataset}", dataset=["mafft.fasta","clustawl.aln","muscle.fasta","muscle-fastest.fasta"])
    output:
        temp(expand("{dataset}", dataset=["alignment_report_clustawl.aln.txt","alignment_report_mafft.fasta.txt","alignment_report_muscle.fasta.txt","alignment_report_muscle-fastest.fasta.txt"]))
    shell:
        "python3 summarize_alignment.py -f {input}"
