FROM snakemake/snakemake:latest

# install multiple bioinformatics programs
# clear cached packages from installed files
# delete .list files from debian package manager

RUN apt-get update && \
    apt-get install -y mafft clustalw muscle && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*


# Install using pip3

RUN pip3 install biopython numpy

# Working directory

WORKDIR /app

 

# Define the command to run when the container starts
CMD ["snakemake", "-j", "all", "-c", "6","--latency-wait","10"]

